\babel@toc {french}{}
\contentsline {section}{\numberline {1}Connaitre les mécanismes d'encapsulation et de fragmentation des différentes couches}{2}{section.1}%
\contentsline {section}{\numberline {2}Etats TCP, connexion, déconnexion, machine d'état, expliquez en détail, numérotation des trames, ACK et SACK.}{6}{section.2}%
\contentsline {section}{\numberline {3}Savoir calculer les tailles des fenêtres TCP et les paramètres d'extension (e.g. Window scaling factor)}{8}{section.3}%
\contentsline {section}{\numberline {4}Algorithmes TCP, "Slow Start", Tamporisateurs TCP}{10}{section.4}%
