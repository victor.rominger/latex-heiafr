\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Objet}{5}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Présentation de l'entreprise Swissdotnet SA }{6}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Organisation de l'entreprise}{6}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Présentation des produits}{6}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1} IP-SPT-SDN-3030 }{7}{subsubsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Le RCT}{7}{subsubsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3}La Mobilisation}{7}{subsubsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.4}CellNet}{8}{subsubsection.2.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Travaux effectués}{9}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Réalisation d'un banc de test}{9}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}Description de la tâche}{10}{subsubsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}Réalisation}{10}{subsubsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.3}Problèmes rencontrés et solutions apportées}{11}{subsubsection.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.4}Enrichissements personnels et apprentissage}{12}{subsubsection.3.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Surveillance sur le long terme de transmetteurs d'alarmes}{12}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Description de la tâche}{12}{subsubsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Réalisation}{12}{subsubsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.3}Enrichissements personnels et apprentissage}{13}{subsubsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Outil d'automatisation d'ajout de données sur le serveur de mobilisation}{14}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.1}Description de la tâche}{14}{subsubsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.2}Réalisation}{14}{subsubsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.3}Problèmes rencontrés et solutions apportées}{14}{subsubsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.4}Enrichissements personnels et apprentissage}{14}{subsubsection.3.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Conclusion}{15}{section.4}
